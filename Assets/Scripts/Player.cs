﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject
{
    public int wallDamage = 1; //damage player applies to wall objs when chopped
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;
    public Text foodText;
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;

    private Animator animator; //stores a reference to our animator component
    private int food; //stores player's score during the lvl before passing it back to the game manager as we change lvls

    // Start is called before the first frame update
    protected override void Start()
    {
        animator = GetComponent<Animator>();
        food = GameManager.instance.playerFoodPoints; //this way player can manage the food score during the lvl
        //and then store it in the game manager
        foodText.text = "Food: " + food;
        base.Start();
    }

    private void OnDisable() //it'll be called when the player GO is disabled to store the food as lvls change
    {
        GameManager.instance.playerFoodPoints = food;

    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.playersTurn) return; //if not true, the following code will not execute

        int horizontal = 0; //use these to store the direction that we're moving, either as 1 or -1
        int vertical = 0;

        horizontal = (int) Input.GetAxisRaw("Horizontal");
        vertical = (int) Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
            vertical = 0;

        if (horizontal != 0 || vertical != 0)
            AttemptMove<Wall> (horizontal,vertical);
    }

    protected override void AttemptMove <T> (int xDir, int yDir)//T specifies the type of component we expect
    //our mover to encounter
    {
        food--; //every move = -1 food points
        
        foodText.text = "Food: " + food;

        base.AttemptMove <T> (xDir, yDir);

        RaycastHit2D hit; //references the result of the line cast done in move
        if (Move (xDir, yDir, out hit))
        {
            SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
        }
        CheckIfGameOver();

        GameManager.instance.playersTurn = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Exit")
        {
            Invoke ("Restart", restartLevelDelay);//calls the restart function 1 sec after colliding with exit
            enabled = false; // disables player
        }
        else if (other.tag == "Food")
        {
            food += pointsPerFood;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            SoundManager.instance.RandomizeSfx(eatSound1, eatSound2);
            other.gameObject.SetActive(false); //makes the GO inactive
        }
        else if (other.tag == "Soda")
        {
            food += pointsPerSoda;
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
            other.gameObject.SetActive(false); //makes the GO inactive
        }

    }

    protected override void OnCantMove <T> (T component)
    {
        Wall hitWall = component as Wall;
        hitWall.DamageWall(wallDamage);
        animator.SetTrigger("playerChop");
    }

    private void Restart()
    {
        SceneManager.LoadScene (0);
    }

    public void LoseFood (int loss)
    {
        animator.SetTrigger("playerHit");
        food -= loss;
        foodText.text = "-" + loss + " Food: " + food;
        CheckIfGameOver();

    }

    private void CheckIfGameOver()
    {
        if (food <= 0)
        {
            SoundManager.instance.PlaySingle(gameOverSound);
            SoundManager.instance.musicSource.Stop();
            GameManager.instance.GameOver(); 
        }
            
    }
}
