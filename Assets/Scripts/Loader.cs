﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{
    public GameObject gameManager;
    void Awake()
    {
        if (GameManager.instance == null)//checks if gameMangager has been instantiated
        Instantiate(gameManager);
    }
}
