﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour
//The abstract keyword enables you to create incomplete classes and class members
// must be implemented in a derived class.
{

    public float moveTime = 0.1f; //time (seconds) it takes obj to move
    public LayerMask blockingLayer; //layer checks collision to determine if a space is open to be moved into

    private BoxCollider2D boxCollider; //the bc2d component attached to obj
    private Rigidbody2D rb2D;
    private float inverseMoveTime; //makes movement calculations more efficient


    // Start is called before the first frame update
    protected virtual void Start()
    //protected virtual functions can be overridden by inheriting classes
    //useful if need different implementation of start in inheriting class
    {
        boxCollider = GetComponent <BoxCollider2D> ();//Get component reference to bc2d and store in boxCollider
        rb2D = GetComponent <Rigidbody2D> ();//-||-
        inverseMoveTime = 1f / moveTime;//storing the opposite of moveTime, it can be used more efficiently
    }

    protected bool Move (int xDir, int yDir, out RaycastHit2D hit) //Move returns true OR false 
    //Move takes parameters for x direction, y direction and a RaycastHit2D to check collision.
    //out causes arguments to be pass by reference, here it's used to return > than 1 value
    {
        Vector2 start = transform.position;//stores the current position under start
        //transform.position is a V3 but by casting it to a V2, we can implicitly convert it
        Vector2 end = start + new Vector2 (xDir, yDir);//calculates the end position based on the parameters
        boxCollider.enabled = false;//disabling bc2d so that the ray doesn't hit this obj's own bc2d
        hit = Physics2D.Linecast (start, end, blockingLayer);//Cast line from start to end checking collision on blockingLayer.
        boxCollider.enabled = true;
        if(hit.transform == null)//check if anything was hit
        //if the space we cast our line into was open
        {
            StartCoroutine (SmoothMovement (end));
            return true; //says that Move was successful
        }
        
        return false;
    }

    protected IEnumerator SmoothMovement (Vector3 end)//co-routine moves units by space of one
    //parameter end specifies where to move to
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;//calculates the remaining moving distance
        //based on the square magnitude of the difference between current position and end parameter
        //Square magnitude instead of magnitude as it's computationally cheaper.

        while(sqrRemainingDistance > float.Epsilon)//checks that sqrRemainingDistance > very tiny # (almost 0)
        {
            Vector3 newPostion = Vector3.MoveTowards(rb2D.position, end, inverseMoveTime * Time.deltaTime);
            //finds new position which is proportionally closer to the end, based on the moveTime
            //Vector3.MoveTowards moves a point (newPosition) (from rb2D.Position) in a straight line towards a target point (end)
            //the value returned by Vector3.MoveTowards is the last parameter

            rb2D.MovePosition (newPostion);//use rb2D to move to the new position
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;//recalculates the remaining distance after moving
            yield return null;//return and loop until sqrRemainingDistance is close enough to 0

        }
    }

    protected virtual void AttemptMove <T> (int xDir, int yDir)
    //AttemptMove takes generic parameter T to specify type of component we expect our unit
    // to interact with if blocked(Player for Enemies, Wall for Player).
        where T : Component //T is going to be component
    {
        RaycastHit2D hit; //stores what linecast hits when Move is called
        bool canMove = Move (xDir, yDir, out hit);//sets canMove to true is Move was successful
        if(hit.transform == null)
                return; //if nothing was hit, return and don't execute further code
        T hitComponent = hit.transform.GetComponent <T> ();
        //Get a component reference to the component of type T attached to the object that was hit

        if(!canMove && hitComponent != null)//meaning that the moving obj is blocked by sth it can hit
            OnCantMove(hitComponent);
    }

    protected abstract void OnCantMove <T> (T component)//the abstarct modifier indicates that the modified
    //has a missing or incomplete implementation
    //OnCantMove will be overriden by functions in the inheriting classes.
        where T : Component;
}
