﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public Sprite dmgSprite;//sprite to display once player has hit the wall
    public int hp = 4; //hit points
    private SpriteRenderer spriteRenderer;
    public AudioClip chopSound1;
    public AudioClip chopSound2;
    // Start is called before the first frame update
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void DamageWall (int loss)
    {
        SoundManager.instance.RandomizeSfx(chopSound1, chopSound2);
        spriteRenderer.sprite = dmgSprite;//set the sprite of our spriteRendered to our damaged sprite
        hp -= loss; //subtracts loss from this wall's current hp total
        if (hp <= 0)
            gameObject.SetActive(false);//disables the GO

    }
}
