﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
    [System.Serializable]
    public class Count
    {
        public int minimun;
        public int maximum;

        public Count (int min, int max) //an assignment constructor for Count
        // so that we can set the values of minimum and maximum when we declare a new Count.
        //parameters min and max are used to set the values of minimum and maximum.
        {
            minimun = min;
            maximum = max;
        }
    }

    public int columns = 8;
    public int rows = 8; //these variables will delineate the dimensions of our game board
    public Count wallCount = new Count (5,9); //use Count to specify a random range for how many walls to spawn in each level
    //this means a minimum of 5 walls per level and maximum of 9 walls per level
    public Count foodCount = new Count (1,5); //-||- for food items
    public GameObject exit; //our exit object

    public GameObject[] floorTiles; //we use arrays so that we can pass in multiple objs
    //and choose the 1 we want to spawn among the variations
    public GameObject[] wallTiles; //-||-
    public GameObject[] foodTiles; //-||-
    public GameObject[] enemyTiles; //-||-
    public GameObject[] outerWallTiles; //-||-
    //these arrays will be filled with our different prefabs between which we can choose in the inspector

    private Transform boardHolder; //we use this to keep the Hierarchy clean by childing our spawned objs to it
    //and collapsing it
    private List <Vector3> gridPositions = new List<Vector3>(); //tracks all of possible position variations on our game board
    //and to track whether an obj has been spawned in that position or not

    void InitialiseList() //function returns void
    {
        gridPositions.Clear(); //clears our list of grid positions

        //these loops fill our list with each of the positions on our game board as a V3 (Vector3)
        //this creates a list of possible positions to place walls, enemies or pickups
        for (int x = 1; x < columns - 1; x++) //this loop is going to run as long as x < columns - 1
        //looping from 1 to columns -1 so a border of floor tiles is left to not create impassable levels 
        {
            for (int y = 1; y < rows -1; y++) //-||- as x axis loop
            {
                gridPositions.Add(new Vector3(x,y,0f)); //add a new V3 with our x and y values to our list gripPositions
            }
        }
    }

    void BoardSetup () //use this to setup the outer wall and the floor of our game board
    {
        boardHolder = new GameObject ("Board").transform; //boardHolder equals the transform of new GO called Board

        for (int x = -1; x < columns + 1; x++) //lays out floor and outerwall tiles
        //looping from -1 to columns +1 to build an edge around the active portion of the game board using the outer wall objects
        {
            for (int y = -1; y < rows + 1; y++)
            {
                GameObject toInstantiate = floorTiles[Random.Range (0, floorTiles.Length)]; //choose floor tile at random between 0 and the length of the respective array and prepare to instantiate it
                //declaring a variable of the type GO called toInstantiate and setting it to equal an index in our floorTiles array
                if (x == -1 || x == columns || y == -1 || y == rows)//check if we're in one of the outer wall positions
                toInstantiate = outerWallTiles[Random.Range (0, outerWallTiles.Length)];//and if so we're going to choose an outer wall tile to instantiate

                //now we're actually instantiating it
                GameObject instance = Instantiate(toInstantiate, new Vector3 (x,y,0f), Quaternion.identity) as GameObject; //Quaternion.identity means no rotation
                //assign the GO instance to the obj that we're instantiating
                //call Instantiate, pass in toInstantiate, the prefab that we chose, at a new V3 which is going to be based on our current X and Y in the loop
                //and the cast it to a GO
                
                instance.transform.SetParent(boardHolder); //set the parent of our newly instantiated GO to boardHolder
            }
        }
    }

    Vector3 RandomPosition ()//returns a V3
    {
        int randomIndex = Random.Range(0, gridPositions.Count); //generate a random # within a range of 0 and the # of our grid positions
        Vector3 randomPosition = gridPositions[randomIndex];// equal the grid position stored in gridPositions list at our randomly selected index 
        gridPositions.RemoveAt(randomIndex); //makes sure not to spawn 2 objs at the same position by removing that grid position from our list
        return randomPosition;//return the value of randomPosition so that we can use it to spawn our object in a random location
    }

    void LayoutObjectAtRandom(GameObject[] tileArray, int minimun, int maximum)//this funtion is going to spawn our tiles at the random position that we've chosen 
    {
        int objectCount = Random.Range (minimun, maximum + 1);//control how many of a given obj we're going to spawn

        for (int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPosition(); //choose random position by calling our RandomPosition function
            GameObject tileChoice = tileArray[Random.Range (0, tileArray.Length)];//choose a random tile from our array of tiles
            Instantiate (tileChoice, randomPosition, Quaternion.identity);//instantiate at our random position
        }
    }

    public void SetupScene(int level)//public cause it's going to be called by th gameManager when it's time to set up the board
    {
        BoardSetup();
        InitialiseList();
        LayoutObjectAtRandom(wallTiles, wallCount.minimun, wallCount.maximum);
        LayoutObjectAtRandom(foodTiles, foodCount.minimun, foodCount.maximum);
        int enemyCount = (int)Mathf.Log(level,2f);
        LayoutObjectAtRandom(enemyTiles, enemyCount, enemyCount);//min and max values are the same because we're not specifing a random range
        Instantiate(exit, new Vector3(columns - 1, rows - 1, 0F), Quaternion.identity);//the exit is always going to be placed in the upper right column of the lvl
    }
}
