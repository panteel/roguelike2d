﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float levelStartDelay = 2f;
    public float turnDelay = .1f;

    public static GameManager instance = null; //a sigleton: an obj for which there can only be 1 instance in the game at any given time
    //declaring it as static means that the variable will belong to the class itself as opposed to an instance in the class
    //means access the public functions and variables this script from any script in our game
    public BoardManager boardScript; //store a reference to BoardManager which will set up the lvl
    public int playerFoodPoints = 100;
    [HideInInspector] public bool playersTurn = true;
    
    private Text levelText;
    private GameObject levelImage;
    private int level = 0;
    private List<Enemy> enemies;
    private bool enemiesMoving;
    private bool doingSetup;//prevent the player from moving when setting up the board


    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null) //check is instance already exists
            instance = this;
        else if (instance != this)
            Destroy(gameObject); //so we don't end up with two instances
        DontDestroyOnLoad(gameObject);//when we load a new scene, normally all of the GOs in the hierarchy will be destroyed
        //bcs we want to keep track of the score between scenes we don't want it destroyed
        enemies = new List<Enemy>();
        boardScript = GetComponent<BoardManager>(); //get and store a component reference to our BoardManager script
        //InitGame(); //call this function to initialize the first lvl
    }

    public void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        //Add one to our level number.
        level++;
        InitGame();
        
    }
    void OnEnable()
    {
        //Tell our ‘OnLevelFinishedLoading’ function to start listening for a scene change event as soon as
        //this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    void OnDisable()
    {
        //Tell our ‘OnLevelFinishedLoading’ function to stop listening for a scene change event as soon as this
        //script is disabled.
        //Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
    void InitGame()
    {
        doingSetup = true;

        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Day " + level; //sets the day to the current lvl
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);

        enemies.Clear();
        boardScript.SetupScene(level); //call the setupscene function of boardscript and pass in the parameter lvl
        //tells boardscript what lvl the scene that we're setting up to determine the # of enemies
    }

    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false;
    }

    public void GameOver ()
    {
        levelText.text = "After " +level+ " days, you starved.";
        levelImage.SetActive(true);
        enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (playersTurn || enemiesMoving || doingSetup)
            return;

        StartCoroutine(MoveEnemies());
    }

    public void AddEnemyToList(Enemy script)//makes enemies register themeselves to the gamemanager
    {
        enemies.Add (script);
    }

    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);
        if (enemies.Count == 0)
        {
            yield return new WaitForSeconds(turnDelay);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].MoveEnemy();
            yield return new WaitForSeconds(enemies[i].moveTime);
        }

        playersTurn = true;
        enemiesMoving = false;

    }
}
