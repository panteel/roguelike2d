﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject
{
    public int playerDamage;

    private Animator animator;
    private Transform target;//stores the player's position
    private bool skipMove; //make enemies move every other turn
    public AudioClip enemyAttack1;
    public AudioClip enemyAttack2;


    protected override void Start()
    {
        GameManager.instance.AddEnemyToList(this);
        animator = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag ("Player").transform;
        base.Start();
    }

    protected override void AttemptMove <T> (int xDir, int yDir)
    {
        if (skipMove)//ckecks to see if enemy is skipping this turn
        {
            skipMove = false;
            return;
        }

        base.AttemptMove <T> (xDir, yDir);

        skipMove = true;
    }

    public void MoveEnemy()//called by gamemanager when it issues the order to move
    {
        int xDir = 0;
        int yDir = 0;

        if (Mathf.Abs (target.position.x - transform.position.x) < float.Epsilon)
        //checks if player and enemy are on same x axis
            yDir = target.position.y > transform.position.y ? 1 : -1;
            //if the player's y coord. > the enemy's y coord., set y direction to 1, if not, set to -1
        else
            xDir = target.position.x > transform.position.x ? 1 : -1;
            //if the player's x coord. > the enemy's x coord., set x direction to 1, if not, set to -1

        AttemptMove <Player> (xDir, yDir);
    }

    protected override void OnCantMove <T> (T component)
    //called if enemy attempts to move into a space occupied by a Player
    //also overrides the OnCantMove function of MovingObj
    {
        Player hitPlayer = component as Player;
        animator.SetTrigger("enemyAttack");
        SoundManager.instance.RandomizeSfx(enemyAttack1, enemyAttack2);
        hitPlayer.LoseFood(playerDamage);

    }
}
